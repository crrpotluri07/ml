import tensorflow, tflearn
import json, random, numpy
import os, sys, datetime
import nltk
from nltk.stem.lancaster import LancasterStemmer
import sklearn
from sklearn.preprocessing import OneHotEncoder

#nltk.download('punkt') #to download the punkt resource from nltk
os.chdir(r'C:\Users\harim\Desktop\bitbucket_crrpotluri07\ml\chatbot') #change the directory

# ---------------------------------------------- data preprocessing ---------------------------------------------
stemmer = LancasterStemmer()
list_words = []
list_patterns = []
list_patterns_tag = []
list_tags = []

#read the data from json
with open('conversation.json') as convo:
    data = json.load(convo)

#get the data into respective lists
for intent in data['intents']:
    for pattern in intent['patterns']:
        pattern_words = nltk.word_tokenize(pattern) # split the phrases to comma seperated words in pattern
        list_words.extend(pattern_words) #append to the list_words
        list_patterns.append(pattern)
        list_patterns_tag.append(intent['tag'])
        
    if intent['tag'] not in list_tags:
        list_tags.append(intent['tag'])

#convert all the words in list_words to lower case
list_words = [stemmer.stem(w.lower()) for w in list_words]
list_words = sorted(list(set(list_words)))

#sort the data in tags
list_tags = sorted(list(set(list_tags)))

#Create BAG OF WORDS aka assign numeric values to the data in lists
