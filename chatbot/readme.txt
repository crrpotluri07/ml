change description : 
	create a basic chatbot that learns and develops based on the interactions using neural network

environment:
	Anaconda 3 (py 3.6)

libraries:
	tensorflow, tflearn, numpy, nltk, json, random, sklearn
	
	please make sure that tensorflow version installed is not v2, as there are some compatibility issues between tensorflow and tflearn
		1) pip install tensorflow==1.13.0rc1
		the command uninstalls the v2, if you already have it installed on your machine, and installs 1.13.0
	
	There is a trick to install "tflearn", as the direct installation doesnt suit our needs
	if you already have tflearn:
		1) pip uninstall tflearn
		2) pip install git+https://github.com/tflearn/tflearn.git
	if you are installing tflearn for the first time:
		1) pip install tensorflow
		2) pip install git+https://github.com/tflearn/tflearn.git