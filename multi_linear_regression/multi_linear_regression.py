#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  2 13:00:15 2020

@author: chaitanyapotluri
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os

os.chdir('/Users/chaitanyapotluri/Desktop/Chaitanya/GIT/ml/multi_linear_regression')

# ---------------------------------- Begin: data preprocessing ------------------------------------------
#take the dataset into a dataframe
dataset = pd.read_csv('50_startups.csv')

#split the dependent and independent dimension
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:,4].values

#assign values for the cities in the dataset
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_X = LabelEncoder()
X[:,3] = labelencoder_X.fit_transform(X[:,3])
onehotencoder = OneHotEncoder(categorical_features =[3])
X = onehotencoder.fit_transform(X).toarray()

#Avoid the Dummy Variable trap
X = X[:, 1:]
# ---------------------------------- End: data preprocessing ------------------------------------------


#split the dataset into testing and training sets
from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(X, y, train_size=0.80, random_state=0)

#fit the training datasets into LinearRegression module
from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(x_train, y_train)

y_pred = regressor.predict(x_test)


#implement the backward elimination to find the most statistically significant dimension
import statsmodels.api as sm #statsmodel library is used to calculate the p-vlaue
X = np.append(arr = np.ones([50,1], dtype = int), values = X, axis = 1)

#calculate the P values of all - this will help us in eliminating the varibale with highest significance level
X_opt = pd.DataFrame(X)
regressor_opt = sm.OLS(endog = y, exog = X_opt).fit()
print(regressor_opt.summary())
#eliminate the next highest P value variable
X_opt = X_opt.drop(2, axis = 1)
regressor_opt = sm.OLS(endog = y, exog = X_opt).fit()
print(regressor_opt.summary())
#eliminate the next highest P value variable
X_opt = X_opt.drop(1, axis = 1)
regressor_opt = sm.OLS(endog = y, exog = X_opt).fit()
print(regressor_opt.summary())
#eliminate the next highest P value variable
X_opt = X_opt.drop(5, axis = 1)
regressor_opt = sm.OLS(endog = y, exog = X_opt).fit()
print(regressor_opt.summary())


