#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 23 17:27:34 2019
@author: chaitanyapotluri

Problem Description : Figure out the co-relation between the years of experience and salary in an organization
Data :  sample dataset with 2 columns - years of experience and salary
To Do: develop a model to establish the corelation between YoE and Salary

math - 
 Simple Linear Regression : y = b(0) + b(1)x(1)
 y - dependent variable
 x(1) - independent variable
 b(0)- point where y crosses x, in this case - the starting salary in the organization with 0 years of experience
 b(1)- coefficient that explains the impact of the unit change of x on y

"""

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sklearn

#set working directory
os.chdir('/Users/chaitanyapotluri/Desktop/Chaitanya/GIT/ML/simple_linear_regression')

#import datatset
df = pd.read_csv('Salary_Data.csv')

#take the independent variable into X
x = df.iloc[:, :-1].values

#take the ependent variable into X
y = df.iloc[:, 1].values

#split the dataset into training and test sets
#20 observations into training  
#10 observations into test
from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(x, y, train_size=0.66, random_state=0)

#apply simple linear regression onto our training datasets
from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
regressor.fit(x_train, y_train)

#predict the Test results
y_pred = regressor.predict(x_test)

################ Visualize Training datasets #######################
#Plot the training dataset
plt.scatter(x_train, y_train, color ='red')
#Plot Regression line for training dataset
plt.plot(x_train, regressor.predict(x_train), color = 'blue')
#Beautify
plt.title('Salary V Experience (training datatset)')
plt.xlabel('Years of Experience')
plt.ylabel('Salary')
plt.show()

################ Visualize Test datasets #######################
#Plot the training dataset
plt.scatter(x_test, y_test, color ='red')
#Plot Regression line for training dataset
plt.plot(x_train, regressor.predict(x_train), color = 'blue')
#Beautify
plt.title('Salary V Experience (test datatset)')
plt.xlabel('Years of Experience')
plt.ylabel('Salary')
plt.show()